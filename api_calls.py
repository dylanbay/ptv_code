###################################################################################
#
#   Filename: api_calls
#
#   Description: The purpose of this code is to generate and run api calls to
#                access information from the PTV swagger API
#
#
#   Author: Dylan Bayley
#
#   Date: 19/06/2017
#
#   Python: 3.5
#
##################################################################################
from hashlib import sha1
import hmac
import binascii
import json
import requests
from pprint import pprint


#include "Python.h"


def handler():
    """ The purpose of this function is to decide which code/function will be run for each api
        call that is requested.
    """
    print('Entered Handler function')

    # Run a health check first
    health_command = '/v3/route_types'
    health_url = get_url(health_command)

    resp = requests.get(health_url)
    error_code = check_response(resp)

    if error_code == 1:
        #let python know its a json response
        resp = resp.json()
        pprint(resp)
    else:
        print("\n \n SERVER REQUEST FAILED \n\n")
        print('The server responded with the error code: ' + str(error_code))

    return

def check_response(resp):
    """ This function checks that response from the server was valid

        input: Response

        Returns:
            True if json data returned
            Error_code if error occurred

    """
    if resp.status_code != requests.codes.ok:
        return resp.status_code
    else:
        return True


def get_url(request):
    """ This function runs produces the signature so that the API call can be
        successfully run.
    """
    devId = 3000281
    key = 'dace067d-c2c1-48ba-abfc-8c424c71ec69'
    request = request + ('&' if ('?' in request) else '?')
    raw = request+'devid={0}'.format(devId)
    hashed = hmac.new(bytearray(key, 'ASCII'), bytearray(raw, 'ASCII'), sha1)
    signature = hashed.hexdigest()
    base = 'http://timetableapi.ptv.vic.gov.au'

    return base+raw+'&signature={1}'.format(devId, signature)

def routes(route_type='', route_name='', route_id=''):
    """ This function builds the string for a routes query
        args: route_type, route_name, route_id

        returns: String for API call

    """
    if (route_type == '') and (route_name == '') and (route_id == ''):
        print('No information was passed through to the routes command. ')
        return '/v3/route_types'



def main():
    """ The main function for the api-caller file

        inputs: None
        outputs: True when code is complete
    """

    print('\nInitialising... \n')
    handler()

    return True

if __name__ == '__main__':
    main()